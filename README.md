## General purpose

This app has various function, including to display random joke, display all categories, display random joke by category selected and display random joke by text you input.

---

## Instrumental testing

The following instrument testing that has been made.

1. Make sure the refresh button with id **btRefreshQuote** appear
2. Giving action of clicking on the refresh button
3. Make sure spinner with id **acCategory** appears
4. Make sure recyclerview with id **rvCategories** appears
5. Giving action of scrolling on the recyclerview to the end of data
6. Giving action of open action bar overflow or option menu
7. Giving action of clicking on option menu with text **Search by text**
8. Giving action of texting of textinputedittext with id **fbSearch** with value is **months** and press ime action / searching
9. Giving action of scrolling on the recyclerview with id **rvJoke** to the end of data
10. Giving action of click on last item from recyclerview
11. Giving action of click on button close with id **btClose**

---

## Unit testing

The following unit testing that has been made.

1. file of  **MainViewModelTest**
- ensure that the data obtained from the **getRandom** process is not null
- ensure that the data obtained from the **getCategories** process is not null
- ensure that the data obtained from the **getRandomJokeByCategory** process is not null

2. file of **SearchJokeViewModelTest**
- ensure that the data obtained from the **getJokeByText** process is not null

3. file of **DefaultRepositoryTest**
- ensure that the data obtained from the **getRandom** process is not null
- ensure that the data obtained from the **getCategories** process is not null
- ensure that the data obtained from the **getJokesByText** process is not null
- ensure that the data obtained from the **getJokeByCategory** process is not null
