package com.hirin.mytestvidio.ui.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.hirin.mytestvidio.data.remote.response.JokesResponse
import com.hirin.mytestvidio.data.repository.DefaultRepository
import com.hirin.mytestvidio.utils.DataDummy
import com.hirin.mytestvidio.vo.Resource
import com.nhaarman.mockitokotlin2.verify
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest {
    private lateinit var mainViewModel: MainViewModel

    @get:Rule
    var instantTaskEnumConstantNotPresentException = InstantTaskExecutorRule()

    @Mock
    private lateinit var defaultRepository: DefaultRepository

    @Before
    fun setUp() {
        mainViewModel = MainViewModel(defaultRepository)
    }

    @Test
    fun getRandom() {
        val dataResponse = Resource.success(DataDummy.getRandom())
        val response = MutableLiveData<Resource<JokesResponse>>()
        response.value = dataResponse

        `when`(defaultRepository.getRandom()).thenReturn(response)
        val randomJoke = mainViewModel.getRandom().value?.data
        verify(defaultRepository).getRandom()
        assertNotNull(randomJoke)
    }

    @Test
    fun getCategories() {
        val dataResponse = Resource.success(DataDummy.getCategories())
        val response = MutableLiveData<Resource<MutableList<String>>>()
        response.value = dataResponse

        `when`(defaultRepository.getCategories()).thenReturn(response)
        val category = mainViewModel.getCategories().value?.data
        verify(defaultRepository).getCategories()
        assertNotNull(category)
    }

    @Test
    fun getRandomJokeByCategory() {
        val name = "animal"
        val dataResponse = Resource.success(DataDummy.getRandomByCategory(name))
        val response = MutableLiveData<Resource<JokesResponse>>()
        response.value = dataResponse

        `when`(defaultRepository.getJokeByCategory(name)).thenReturn(response)
        val jokeCategory = mainViewModel.getRandomJokeByCategory(name).value?.data
        verify(defaultRepository).getJokeByCategory(name)
        assertNotNull(jokeCategory)
    }
}