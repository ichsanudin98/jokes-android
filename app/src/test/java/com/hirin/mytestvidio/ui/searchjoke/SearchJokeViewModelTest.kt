package com.hirin.mytestvidio.ui.searchjoke

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.hirin.mytestvidio.data.remote.response.SearchJokesTextResponse
import com.hirin.mytestvidio.data.repository.DefaultRepository
import com.hirin.mytestvidio.utils.DataDummy
import com.hirin.mytestvidio.vo.Resource
import com.nhaarman.mockitokotlin2.verify
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SearchJokeViewModelTest {
    private lateinit var searchJokeViewModel: SearchJokeViewModel

    @get:Rule
    var instantTaskEnumConstantNotPresentException = InstantTaskExecutorRule()

    @Mock
    private lateinit var defaultRepository: DefaultRepository

    @Before
    fun setUp() {
        searchJokeViewModel = SearchJokeViewModel(defaultRepository)
    }

    @Test
    fun getJokeByText() {
        val text = "months"
        val dataResponse = Resource.success(DataDummy.getJokeByText(text))
        val response = MutableLiveData<Resource<SearchJokesTextResponse>>()
        response.value = dataResponse

        Mockito.`when`(defaultRepository.getJokesByText(text)).thenReturn(response)
        val jokeByText = searchJokeViewModel.getJokeByText(text).value?.data
        verify(defaultRepository).getRandom()
        assertNotNull(jokeByText)
    }
}