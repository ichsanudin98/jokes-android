package com.hirin.mytestvidio.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.hirin.mytestvidio.data.remote.RemoteDataSource
import com.hirin.mytestvidio.utils.DataDummy
import junit.framework.Assert.assertNotNull
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock

class DefaultRepositoryTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val remote = mock(RemoteDataSource::class.java)

    @Test
    fun getRandom() {
        val randomJoke = DataDummy.getRandom()
        assertNotNull(randomJoke)
    }

    @Test
    fun getCategories() {
        val categories = DataDummy.getCategories()
        assertNotNull(categories)
    }

    @Test
    fun getJokesByText(text: String) {
        val jokeByText = DataDummy.getJokeByText(text)
        assertNotNull(jokeByText)
    }

    @Test
    fun getJokeByCategory(category: String) {
        val randomJoke = DataDummy.getRandomByCategory(category)
        assertNotNull(randomJoke)
    }
}