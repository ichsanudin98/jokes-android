package com.hirin.mytestvidio.ui

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import com.hirin.mytestvidio.R
import com.hirin.mytestvidio.ui.main.MainActivity
import com.hirin.mytestvidio.utils.DataDummy
import com.hirin.mytestvidio.utils.EspressoIdlingResource
import org.junit.*
import org.junit.runners.MethodSorters

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class MainActivityTest {
    private val randomJoke = DataDummy.getRandom()
    private val category = "animal"
    private val randomCategory = DataDummy.getRandomByCategory(category)
    private val text = "months"
    private val jokeByText = DataDummy.getJokeByText(text)
    private val categories = DataDummy.getCategories()

    @get:Rule
    var activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun setup() {
        IdlingRegistry.getInstance().register(EspressoIdlingResource.getEspressoIdlingResource())
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.getEspressoIdlingResource())
    }

    @Test
    fun uiTesting() {
        onView(withId(R.id.btRefreshQuote)).check(matches(isDisplayed()))
        onView(withId(R.id.btRefreshQuote)).perform(click())

        onView(withId(R.id.acCategory)).check(matches(isDisplayed()))

        onView(withId(R.id.rvCategories)).check(matches(isDisplayed()))
        onView(withId(R.id.rvCategories)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(categories.size))

        openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)
        onView(withText(R.string.tl_search_joke_text)).perform(click())

        onView(withId(R.id.fbSearch)).perform(typeText("months"), pressImeActionButton())

        onView(withId(R.id.rvJoke)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(jokeByText.jokeResponse.size))
        onView(withId(R.id.rvJoke)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(jokeByText.jokeResponse.size - 1, click()))

        onView(withId(R.id.btClose)).perform(click())
    }
}