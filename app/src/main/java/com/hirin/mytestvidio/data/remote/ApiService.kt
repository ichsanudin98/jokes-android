package com.hirin.mytestvidio.data.remote

import androidx.lifecycle.LiveData
import com.hirin.mytestvidio.BuildConfig
import com.hirin.mytestvidio.data.remote.response.JokesResponse
import com.hirin.mytestvidio.data.remote.response.SearchJokesTextResponse
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ApiService {
    @Headers("Content-Type: application/json")
    @GET(BuildConfig.URL_RANDOM)
    fun getRandom(): LiveData<ApiResponse<JokesResponse>>

    @Headers("Content-Type: application/json")
    @GET(BuildConfig.URL_CATEGORIES)
    fun getCategories(): LiveData<ApiResponse<MutableList<String>>>

    @Headers("Content-Type: application/json")
    @GET(BuildConfig.URL_JOKES_BY_SEACH)
    fun getJokesByText(@Query("query") query: String): LiveData<ApiResponse<SearchJokesTextResponse>>

    @Headers("Content-Type: application/json")
    @GET(BuildConfig.URL_RANDOM)
    fun getJokeByCategory(@Query("category") query: String): LiveData<ApiResponse<JokesResponse>>
}