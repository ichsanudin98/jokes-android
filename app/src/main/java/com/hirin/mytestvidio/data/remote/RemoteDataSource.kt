package com.hirin.mytestvidio.data.remote

import androidx.lifecycle.LiveData
import com.hirin.mytestvidio.data.remote.response.JokesResponse
import com.hirin.mytestvidio.data.remote.response.SearchJokesTextResponse
import javax.inject.Inject

class RemoteDataSource @Inject constructor(
    private val apiService: ApiService
): ApiHelper {
    override fun getRandom(): LiveData<ApiResponse<JokesResponse>> {
        return apiService.getRandom()
    }

    override fun getCategories(): LiveData<ApiResponse<MutableList<String>>> {
        return apiService.getCategories()
    }

    override fun getJokesByText(text: String): LiveData<ApiResponse<SearchJokesTextResponse>> {
        return apiService.getJokesByText(text)
    }

    override fun getJokeByCategory(category: String): LiveData<ApiResponse<JokesResponse>> {
        return apiService.getJokeByCategory(category)
    }

}