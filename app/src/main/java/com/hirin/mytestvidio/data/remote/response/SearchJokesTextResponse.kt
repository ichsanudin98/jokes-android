package com.hirin.mytestvidio.data.remote.response

import com.google.gson.annotations.SerializedName

data class SearchJokesTextResponse (
    @SerializedName("total")
    val total: Int,
    @SerializedName("result")
    val jokeResponse: List<JokesResponse>
)