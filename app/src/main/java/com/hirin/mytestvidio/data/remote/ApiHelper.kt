package com.hirin.mytestvidio.data.remote

import androidx.lifecycle.LiveData
import com.hirin.mytestvidio.data.remote.response.JokesResponse
import com.hirin.mytestvidio.data.remote.response.SearchJokesTextResponse

interface ApiHelper {
    fun getRandom(): LiveData<ApiResponse<JokesResponse>>
    fun getCategories(): LiveData<ApiResponse<MutableList<String>>>
    fun getJokesByText(text: String): LiveData<ApiResponse<SearchJokesTextResponse>>
    fun getJokeByCategory(category: String): LiveData<ApiResponse<JokesResponse>>
}