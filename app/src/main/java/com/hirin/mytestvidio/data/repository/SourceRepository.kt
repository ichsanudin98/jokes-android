package com.hirin.mytestvidio.data.repository

import androidx.lifecycle.LiveData
import com.hirin.mytestvidio.data.remote.response.JokesResponse
import com.hirin.mytestvidio.data.remote.response.SearchJokesTextResponse
import com.hirin.mytestvidio.vo.Resource

interface SourceRepository {
    fun getRandom(): LiveData<Resource<JokesResponse>>
    fun getCategories(): LiveData<Resource<MutableList<String>>>
    fun getJokesByText(text: String): LiveData<Resource<SearchJokesTextResponse>>
    fun getJokeByCategory(category: String): LiveData<Resource<JokesResponse>>
}