package com.hirin.mytestvidio.data.repository

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.hirin.mytestvidio.data.remote.ApiEmptyResponse
import com.hirin.mytestvidio.data.remote.ApiErrorResponse
import com.hirin.mytestvidio.data.remote.ApiResponse
import com.hirin.mytestvidio.data.remote.ApiSuccessResponse
import com.hirin.mytestvidio.utils.AppExecutors
import com.hirin.mytestvidio.vo.Resource
import timber.log.Timber

abstract class NetworkOnlyBoundResource<ResultType, RequestType>
@MainThread constructor(private val mExecutors: AppExecutors) {

    private val result = MediatorLiveData<Resource<RequestType>>()

    init {
        result.value = Resource.loading(null)

        fetchFromNetwork()
    }

    @MainThread
    private fun setValue(newValue: Resource<RequestType>) {
        if (result.value != newValue) {
            result.value = newValue
        }
    }

    private fun fetchFromNetwork() {

        val apiResponse = createCall()

        setValue(Resource.loading(null))

        result.addSource(apiResponse) { response ->
            result.removeSource(apiResponse)
            when (response) {
                is ApiSuccessResponse -> {
                    mExecutors.diskIO().execute {
                        saveCallResult(processResponse(response))
                        mExecutors.mainThread().execute {
                            setValue(Resource.success(response.body))
                        }
                    }
                }
                is ApiEmptyResponse -> {
                    Timber.i("response data is empty")
                    onFetchFailed()
                    mExecutors.mainThread().execute {
                        setValue(Resource.error("response data is empty", null))
                    }
                }
                is ApiErrorResponse -> {
                    onFetchFailed()
                    mExecutors.mainThread().execute {
                        setValue(Resource.error(response.errorMessage, null))
                    }
                }
            }
        }
    }

    fun asLiveData() = result as LiveData<Resource<RequestType>>

    @WorkerThread
    protected open fun processResponse(response: ApiSuccessResponse<RequestType>) = response.body

    @MainThread
    protected abstract fun createCall(): LiveData<ApiResponse<RequestType>>

    @WorkerThread
    protected abstract fun saveCallResult(data: RequestType)

    protected open fun onFetchFailed() {}
}