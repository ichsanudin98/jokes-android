package com.hirin.mytestvidio.data.repository

import androidx.lifecycle.LiveData
import com.hirin.mytestvidio.data.remote.ApiResponse
import com.hirin.mytestvidio.data.remote.RemoteDataSource
import com.hirin.mytestvidio.data.remote.response.JokesResponse
import com.hirin.mytestvidio.data.remote.response.SearchJokesTextResponse
import com.hirin.mytestvidio.utils.AppExecutors
import com.hirin.mytestvidio.utils.EspressoIdlingResource
import com.hirin.mytestvidio.vo.Resource
import javax.inject.Inject

class DefaultRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val appExecutors: AppExecutors
): SourceRepository {
    override fun getRandom(): LiveData<Resource<JokesResponse>> {
        EspressoIdlingResource.increment()
        return object : NetworkOnlyBoundResource<JokesResponse, JokesResponse>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<JokesResponse>> {
                return remoteDataSource.getRandom()
            }

            override fun saveCallResult(data: JokesResponse) {
                EspressoIdlingResource.decrement()
            }
        }.asLiveData()
    }

    override fun getCategories(): LiveData<Resource<MutableList<String>>> {
        EspressoIdlingResource.increment()
        return object : NetworkOnlyBoundResource<MutableList<String>, MutableList<String>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<MutableList<String>>> {
                return remoteDataSource.getCategories()
            }

            override fun saveCallResult(data: MutableList<String>) {
                EspressoIdlingResource.decrement()
            }
        }.asLiveData()
    }

    override fun getJokesByText(text: String): LiveData<Resource<SearchJokesTextResponse>> {
        EspressoIdlingResource.increment()
        return object : NetworkOnlyBoundResource<SearchJokesTextResponse, SearchJokesTextResponse>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<SearchJokesTextResponse>> {
                return remoteDataSource.getJokesByText(text)
            }

            override fun saveCallResult(data: SearchJokesTextResponse) {
                EspressoIdlingResource.decrement()
            }
        }.asLiveData()
    }

    override fun getJokeByCategory(category: String): LiveData<Resource<JokesResponse>> {
        EspressoIdlingResource.increment()
        return object : NetworkOnlyBoundResource<JokesResponse, JokesResponse>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<JokesResponse>> {
                return remoteDataSource.getJokeByCategory(category)
            }

            override fun saveCallResult(data: JokesResponse) {
                EspressoIdlingResource.decrement()
            }
        }.asLiveData()
    }
}