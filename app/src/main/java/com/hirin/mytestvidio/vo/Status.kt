package com.hirin.mytestvidio.vo

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}