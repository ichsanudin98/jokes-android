package com.hirin.mytestvidio.utils

import com.hirin.mytestvidio.data.remote.response.JokesResponse
import com.hirin.mytestvidio.data.remote.response.SearchJokesTextResponse

object DataDummy {
    fun getRandom(): JokesResponse {
        return JokesResponse(
            mutableListOf(),
            "2020-01-05 13:42:29.569033",
            "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
            "lFmWlJ5FQq246RGVDBn00w",
            "2020-01-05 13:42:29.569033",
            "https://api.chucknorris.io/jokes/lFmWlJ5FQq246RGVDBn00w",
            "Chuck Norris digests His Food In His Mouth."
        )
    }

    fun getCategories(): MutableList<String> {
        val categories = mutableListOf<String>()
        categories.add("animal")
        categories.add("career")
        categories.add("celebrity")
        categories.add("dev")
        categories.add("explicit")
        categories.add("fashion")
        categories.add("food")
        categories.add("history")
        categories.add("money")
        categories.add("movie")
        categories.add("music")
        categories.add("political")
        categories.add("religion")
        categories.add("science")
        categories.add("sport")
        categories.add("travel")
        return categories
    }

    fun getRandomByCategory(text: String): JokesResponse {
        return JokesResponse(
            mutableListOf(
                "animal"
            ),
            "2020-01-05 13:42:19.104863",
            "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
            "o0sukejatqchi7oyjms6mw",
            "2020-01-05 13:42:19.104863",
            "https://api.chucknorris.io/jokes/o0sukejatqchi7oyjms6mw",
            "Chuck Norris can set ants on fire with a magnifying glass. At night."
        )
    }

    fun getJokeByText(text: String): SearchJokesTextResponse {
        return SearchJokesTextResponse(
            27,
            mutableListOf(
                JokesResponse(
                    mutableListOf(),
                    "2020-01-05 13:42:18.823766",
                    "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                    "1Kr6Jq-gTSi9NYjU9FJjcg",
                    "2020-01-05 13:42:18.823766",
                    "https://api.chucknorris.io/jokes/1Kr6Jq-gTSi9NYjU9FJjcg",
                    "Chuck Norris once trew his used condom in the toilet 9months later the ninja turtles was born"
                ),
                JokesResponse(
                    mutableListOf(
                        "explicit"
                    ),
                    "2020-01-05 13:42:19.104863",
                    "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                    "ukrzhvanszmegxo5jlyxna",
                    "2020-01-05 13:42:19.104863",
                    "https://api.chucknorris.io/jokes/ukrzhvanszmegxo5jlyxna",
                    "Chuck Norris' sperm is so badass, he had sex with Nicole Kidman, and 7 months later she prematurely gave birth to a Ford Excursion."
                )
            )
        )
    }
}