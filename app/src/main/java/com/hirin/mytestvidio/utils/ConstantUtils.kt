package com.hirin.mytestvidio.utils

class ConstantUtils {
    object ApplicationConfig {
       const val NETWORK_ERROR = 98
       const val RESPONSE_ERROR = 99
    }

    object PassingDataKey {
        const val ALERT_VISIBILITY = "alert_visibility"
        const val TITLE = "title"
        const val DESCRIPTION = "description"
    }
}