package com.hirin.mytestvidio.ui.randomquotecategory

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import coil.load
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.hirin.mytestvidio.R
import com.hirin.mytestvidio.data.remote.response.JokesResponse
import com.hirin.mytestvidio.databinding.FragmentRandomQuoteCategoryBottomSheetBinding
import com.hirin.mytestvidio.ui.base.BaseInterface
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RandomQuoteCategoryBottomSheet : BottomSheetDialogFragment(),
    View.OnClickListener, BaseInterface {
    // <editor-fold defaultstate="collapsed" desc="initialize text">
    private lateinit var tlClose: String

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="initialize data">
    private lateinit var randomQuoteCategoryBinding: FragmentRandomQuoteCategoryBottomSheetBinding

    private lateinit var vlData: JokesResponse
    private lateinit var  vlListener: RandomQuoteCategoryCallback
    // </editor-fold>

    companion object {
        @JvmStatic
        fun newInstance(
            data: JokesResponse,
            listener: RandomQuoteCategoryCallback
        ) =
            RandomQuoteCategoryBottomSheet().apply {
                vlData = data
                vlListener = listener
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        randomQuoteCategoryBinding = FragmentRandomQuoteCategoryBottomSheetBinding.inflate(inflater, container, false)
        isCancelable = false
        return randomQuoteCategoryBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initText()
        initUI()
        initData()
    }

    override fun onClick(p0: View?) {
        if (randomQuoteCategoryBinding.btClose == p0) {
            vlListener.onCloseRandomQuoteCategory()
        }
    }

    override fun initText() {
        tlClose = resources.getString(R.string.tl_close)
    }

    override fun initUI() {
        randomQuoteCategoryBinding.btClose.text = tlClose
        randomQuoteCategoryBinding.btClose.setOnClickListener(this)
    }

    override fun initData() {
        randomQuoteCategoryBinding.igIcon.load(vlData.iconUrl)
        randomQuoteCategoryBinding.tbJoke.text = vlData.value
    }
}