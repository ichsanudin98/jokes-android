package com.hirin.mytestvidio.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.hirin.mytestvidio.data.remote.response.JokesResponse
import com.hirin.mytestvidio.data.repository.SourceRepository
import com.hirin.mytestvidio.vo.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: SourceRepository
): ViewModel() {
    fun getRandom(): LiveData<Resource<JokesResponse>> = repository.getRandom()
    fun getCategories(): LiveData<Resource<MutableList<String>>> = repository.getCategories()
    fun getRandomJokeByCategory(category: String): LiveData<Resource<JokesResponse>> = repository.getJokeByCategory(category)
}