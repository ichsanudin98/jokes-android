package com.hirin.mytestvidio.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.hirin.mytestvidio.R
import com.hirin.mytestvidio.data.remote.response.JokesResponse
import com.hirin.mytestvidio.databinding.ActivityMainBinding
import com.hirin.mytestvidio.ui.alert.AlertBottomSheet
import com.hirin.mytestvidio.ui.base.BaseActivity
import com.hirin.mytestvidio.ui.base.BaseInterface
import com.hirin.mytestvidio.ui.main.adapter.CategoryListAdapter
import com.hirin.mytestvidio.ui.randomquotecategory.RandomQuoteCategoryBottomSheet
import com.hirin.mytestvidio.ui.randomquotecategory.RandomQuoteCategoryCallback
import com.hirin.mytestvidio.ui.searchjoke.SearchJokeActivity
import com.hirin.mytestvidio.vo.Resource
import com.hirin.mytestvidio.vo.Status
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity(), BaseInterface,
    View.OnClickListener, MainCallback, RandomQuoteCategoryCallback {
    // <editor-fold defaultstate="collapsed" desc="initialize ui">
    private var alertBottomSheet: AlertBottomSheet? = null
    private var randomQuoteCategoryBottomSheet: RandomQuoteCategoryBottomSheet? = null
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="initialize text">
    private lateinit var tlRandomQuote: String
    private lateinit var tlCategoryList: String
    private lateinit var tlEmpty: String
    private lateinit var tlRandomQuoteCategory: String

    private lateinit var erNetwork: String
    private lateinit var erSystemError: String
    private lateinit var erResponseNull: String
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="initialize data">
    private lateinit var activityMainBinding: ActivityMainBinding

    private lateinit var mainViewModel: MainViewModel

    private lateinit var categoryListAdapter: CategoryListAdapter

    private var goTo: Intent? = null
    // </editor-fold>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(activityMainBinding.root)
        initText()
        initUI()
        initData()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.opt_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.mnSearch -> {
                goTo = Intent(this, SearchJokeActivity::class.java)
                startActivity(goTo)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun initText() {
        tlRandomQuote = resources.getString(R.string.tl_random_quote)
        tlCategoryList = resources.getString(R.string.tl_category_list)
        tlEmpty = resources.getString(R.string.tl_empty)
        tlRandomQuoteCategory = resources.getString(R.string.tl_random_quote_category)

        erNetwork = resources.getString(R.string.er_network1)
        erSystemError = resources.getString(R.string.er_system_error)
        erResponseNull = resources.getString(R.string.er_response_null)
    }

    override fun initUI() {
        activityMainBinding.tbRandomQuote.text = tlRandomQuote
        activityMainBinding.tbCategory.text = tlRandomQuoteCategory
        activityMainBinding.tbCategoryList.text = tlCategoryList

        activityMainBinding.btRefreshQuote.setOnClickListener(this)
    }

    override fun initData() {
        categoryListAdapter = CategoryListAdapter(this)

        with(activityMainBinding.rvCategories) {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = categoryListAdapter
        }

        mainViewModel = ViewModelProvider(this)[MainViewModel::class.java]

        mainViewModel.getRandom().observe(this, getRandomObserver)

        mainViewModel.getCategories().observe(this, {
            when(it.status) {
                Status.SUCCESS -> {
                    it.data?.let { data ->
                        categoryListAdapter.addAll(data)

                        val adapter: ArrayAdapter<String> = ArrayAdapter(this, R.layout.items_text, data)
                        activityMainBinding.acCategory.setAdapter(adapter)
                        activityMainBinding.acCategory.onItemClickListener =
                            AdapterView.OnItemClickListener { parent, arg1, pos, id ->
                                onItemCategoryCallback(data[pos])
                            }

                    }
                }
                Status.LOADING -> {

                }
                Status.ERROR -> {

                }
            }
        })
    }

    override fun onClick(p0: View?) {
        if (activityMainBinding.btRefreshQuote == p0) {
            mainViewModel.getRandom().observe(this, getRandomObserver)
        }
    }

    override fun onItemCategoryCallback(name: String) {
        if (randomQuoteCategoryBottomSheet == null) {
            mainViewModel.getRandomJokeByCategory(name).observe(this, {
                it.data?.let { response ->
                    randomQuoteCategoryBottomSheet = RandomQuoteCategoryBottomSheet.newInstance(
                        response,
                        this@MainActivity
                    )
                    randomQuoteCategoryBottomSheet!!.isCancelable = false
                    randomQuoteCategoryBottomSheet!!.show(supportFragmentManager, randomQuoteCategoryBottomSheet!!.tag)
                }
            })
        }
    }

    override fun onCloseRandomQuoteCategory() {
        randomQuoteCategoryBottomSheet?.let {
            it.dismiss()
            randomQuoteCategoryBottomSheet = null
        }
    }

    private val getRandomObserver = Observer<Resource<JokesResponse>> {
        when(it.status) {
            Status.SUCCESS -> {
                activityMainBinding.igIcon.load(it.data?.iconUrl)
                activityMainBinding.tbJoke.text = it.data?.value
            }
//            Status.LOADING -> activityListBinding.pbLoading.visibility = View.VISIBLE
            Status.ERROR -> {
                /*activityListBinding.pbLoading.visibility = View.GONE
                val data = HashMap<String, Any>()
                data[ConstantUtils.PassingDataKey.TYPE] = ConstantUtils.ApplicationConfig.NETWORK_SYSTEM_ERROR
                data[ConstantUtils.PassingDataKey.TITLE] = tlFailed
                data[ConstantUtils.PassingDataKey.ALERT_VISIBILITY] = true
                data[ConstantUtils.PassingDataKey.DESCRIPTION] =
                    erResponseNull
                goTo(ConstantUtils.GotoCode.PG_ALERT, data)*/
            }
        }
    }
}