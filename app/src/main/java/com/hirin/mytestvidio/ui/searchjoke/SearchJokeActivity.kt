package com.hirin.mytestvidio.ui.searchjoke

import android.os.Bundle
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.hirin.mytestvidio.R
import com.hirin.mytestvidio.data.remote.response.JokesResponse
import com.hirin.mytestvidio.databinding.ActivitySearchJokeBinding
import com.hirin.mytestvidio.ui.alert.AlertBottomSheet
import com.hirin.mytestvidio.ui.base.BaseActivity
import com.hirin.mytestvidio.ui.base.BaseInterface
import com.hirin.mytestvidio.ui.randomquotecategory.RandomQuoteCategoryBottomSheet
import com.hirin.mytestvidio.ui.randomquotecategory.RandomQuoteCategoryCallback
import com.hirin.mytestvidio.ui.searchjoke.adapter.SearchJokeAdapter
import com.hirin.mytestvidio.vo.Status
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchJokeActivity : BaseActivity(),
    BaseInterface, SearchJokeCallback, RandomQuoteCategoryCallback {
    // <editor-fold defaultstate="collapsed" desc="initialize ui">
    private var alertBottomSheet: AlertBottomSheet? = null
    private var randomQuoteCategoryBottomSheet: RandomQuoteCategoryBottomSheet? = null
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="initialize text">
    private lateinit var tlSearchByText: String
    private lateinit var tlEmpty: String

    private lateinit var erNetwork: String
    private lateinit var erSystemError: String
    private lateinit var erResponseNull: String
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="initialize data">
    private lateinit var activitySearchJokeBinding: ActivitySearchJokeBinding

    private lateinit var searchJokeViewModel: SearchJokeViewModel

    private lateinit var searchJokeAdapter: SearchJokeAdapter
    // </editor-fold>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activitySearchJokeBinding = ActivitySearchJokeBinding.inflate(layoutInflater)
        setContentView(activitySearchJokeBinding.root)
        initText()
        initUI()
        initData()
    }

    override fun initText() {
        tlSearchByText = resources.getString(R.string.tl_search_joke_text)
        tlEmpty = resources.getString(R.string.tl_empty)

        erNetwork = resources.getString(R.string.er_network1)
        erSystemError = resources.getString(R.string.er_system_error)
        erResponseNull = resources.getString(R.string.er_response_null)
    }

    override fun initUI() {
        setSupportActionBar(activitySearchJokeBinding.toolbar)
        activitySearchJokeBinding.toolbar.setNavigationOnClickListener { onBackPressed() }
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        activitySearchJokeBinding.tfSearch.hint = tlSearchByText

        activitySearchJokeBinding.fbSearch.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                // Your piece of code on keyboard search click
                searchJokeViewModel.getJokeByText(v.text.toString().trim()).observe(this@SearchJokeActivity, {
                    when(it.status) {
                        Status.SUCCESS -> {
                            it.data?.let { data ->
                                searchJokeAdapter.addAll(data.jokeResponse)
                            }
                        }
                        Status.LOADING -> {

                        }
                        Status.ERROR -> {

                        }
                    }
                })
                return@OnEditorActionListener true
            }
            false
        })
    }

    override fun initData() {
        searchJokeAdapter = SearchJokeAdapter(this)

        with(activitySearchJokeBinding.rvJoke) {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = searchJokeAdapter
        }

        searchJokeViewModel = ViewModelProvider(this)[SearchJokeViewModel::class.java]
    }

    override fun onItemCategoryCallback(jokesResponse: JokesResponse) {
        if (randomQuoteCategoryBottomSheet == null) {
            randomQuoteCategoryBottomSheet = RandomQuoteCategoryBottomSheet.newInstance(
                jokesResponse,
                this@SearchJokeActivity
            )
            randomQuoteCategoryBottomSheet!!.isCancelable = false
            randomQuoteCategoryBottomSheet!!.show(supportFragmentManager, randomQuoteCategoryBottomSheet!!.tag)
        }
    }

    override fun onCloseRandomQuoteCategory() {
        randomQuoteCategoryBottomSheet?.let {
            it.dismiss()
            randomQuoteCategoryBottomSheet = null
        }
    }
}