package com.hirin.mytestvidio.ui.searchjoke

import com.hirin.mytestvidio.data.remote.response.JokesResponse

interface SearchJokeCallback {
    fun onItemCategoryCallback(jokesResponse: JokesResponse)
}