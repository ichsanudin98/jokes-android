package com.hirin.mytestvidio.ui.base

interface BaseInterface {
    fun initText()
    fun initUI()
    fun initData()
}