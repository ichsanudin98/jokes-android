package com.hirin.mytestvidio.ui.alert

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.hirin.mytestvidio.R
import com.hirin.mytestvidio.databinding.FragmentAlertBottomSheetBinding
import com.hirin.mytestvidio.ui.base.BaseInterface
import com.hirin.mytestvidio.utils.ConstantUtils
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AlertBottomSheet : BottomSheetDialogFragment(),
    View.OnClickListener, BaseInterface {
    // <editor-fold defaultstate="collapsed" desc="initialize text">
    private var tlTitle: String? = null
    private lateinit var tlDescription: String
    private lateinit var tlClose: String

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="initialize data">
    private lateinit var alertBottomSheetBinding: FragmentAlertBottomSheetBinding

    private var vlType = 0

    private var vlData: HashMap<String, Any>? = null
    private lateinit var  vlListener: AlertCallback
    // </editor-fold>

    companion object {
        @JvmStatic
        fun newInstance(type: Int,
                        data: HashMap<String, Any>,
                        listener: AlertCallback) =
            AlertBottomSheet().apply {
                vlType = type
                vlData = data
                tlTitle = data[ConstantUtils.PassingDataKey.TITLE].toString()
                tlDescription = data[ConstantUtils.PassingDataKey.DESCRIPTION].toString()
                vlListener = listener
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        alertBottomSheetBinding =
            FragmentAlertBottomSheetBinding.inflate(inflater, container, false)
        isCancelable = false
        return alertBottomSheetBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initText()
        initUI()
        initData()
    }

    override fun onClick(p0: View?) {
        if (alertBottomSheetBinding.btSend == p0) {
            vlListener.onCloseAlert(vlType, vlData)
        }
    }

    override fun initText() {
        tlClose = resources.getString(R.string.tl_close)
    }

    override fun initUI() {
        alertBottomSheetBinding.tbTitle.text = tlTitle
        alertBottomSheetBinding.tbDescription.text = tlDescription

        alertBottomSheetBinding.btSend.text = tlClose
        alertBottomSheetBinding.btSend.setOnClickListener(this)
    }

    override fun initData() {}
}