package com.hirin.mytestvidio.ui.searchjoke

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.hirin.mytestvidio.data.remote.response.SearchJokesTextResponse
import com.hirin.mytestvidio.data.repository.SourceRepository
import com.hirin.mytestvidio.vo.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SearchJokeViewModel @Inject constructor(
    private val repository: SourceRepository
): ViewModel() {
    fun getJokeByText(text: String): LiveData<Resource<SearchJokesTextResponse>> = repository.getJokesByText(text)
}