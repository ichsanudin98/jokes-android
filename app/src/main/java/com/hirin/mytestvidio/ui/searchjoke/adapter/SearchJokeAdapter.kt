package com.hirin.mytestvidio.ui.searchjoke.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.hirin.mytestvidio.data.remote.response.JokesResponse
import com.hirin.mytestvidio.databinding.ItemsJokeBinding
import com.hirin.mytestvidio.ui.searchjoke.SearchJokeCallback

class SearchJokeAdapter constructor(
    val callback: SearchJokeCallback
):RecyclerView.Adapter<SearchJokeAdapter.ViewHolder>() {
    private var allData: List<JokesResponse> = mutableListOf()

    inner class ViewHolder(
        private val binding: ItemsJokeBinding
    ): RecyclerView.ViewHolder(binding.root) {
        fun bind(jokesResponse: JokesResponse) {
            binding.apply {
                igIcon.load(jokesResponse.iconUrl)
                tbJoke.text = jokesResponse.value
                cvRandomJoke.setOnClickListener {
                    callback.onItemCategoryCallback(jokesResponse)
                }
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(allData[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemsJokeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return allData.size
    }

    fun addAll(allData: List<JokesResponse>) {
        this.allData = allData
        notifyDataSetChanged()
    }
}