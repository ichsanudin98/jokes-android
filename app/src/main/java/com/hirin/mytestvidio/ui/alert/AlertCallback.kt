package com.hirin.mytestvidio.ui.alert

interface AlertCallback {
    fun onCloseAlert(type: Int, data: HashMap<String, Any>?)
}