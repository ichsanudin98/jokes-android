package com.hirin.mytestvidio.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hirin.mytestvidio.databinding.ItemsCategoryBinding
import com.hirin.mytestvidio.ui.main.MainCallback

class CategoryListAdapter constructor(
    val callback: MainCallback
):RecyclerView.Adapter<CategoryListAdapter.ViewHolder>() {
    private var allData: MutableList<String> = mutableListOf()

    inner class ViewHolder(
        private val binding: ItemsCategoryBinding
    ): RecyclerView.ViewHolder(binding.root) {
        fun bind(name: String) {
            binding.apply {
                tbName.text = name
                vwParent.setOnClickListener {
                    callback.onItemCategoryCallback(name)
                }
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(allData[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemsCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return allData.size
    }

    fun addAll(allData: MutableList<String>) {
        this.allData = allData
        notifyDataSetChanged()
    }
}