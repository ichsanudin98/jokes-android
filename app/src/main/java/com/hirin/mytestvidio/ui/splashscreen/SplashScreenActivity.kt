package com.hirin.mytestvidio.ui.splashscreen

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.hirin.mytestvidio.R
import com.hirin.mytestvidio.databinding.ActivitySplashScreenBinding
import com.hirin.mytestvidio.ui.base.BaseActivity
import com.hirin.mytestvidio.ui.base.BaseInterface
import com.hirin.mytestvidio.ui.main.MainActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashScreenActivity : BaseActivity(), BaseInterface {
    // <editor-fold defaultstate="collapsed" desc="initialize text">
    private lateinit var tlAppName: String
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="initialize data">
    private lateinit var splashScreenBinding: ActivitySplashScreenBinding
    // </editor-fold>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        splashScreenBinding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(splashScreenBinding.root)
        initText()
        initUI()
        initData()
    }

    override fun initText() {
        tlAppName = resources.getString(R.string.app_name)
    }

    override fun initUI() {}

    override fun initData() {
        Handler(Looper.getMainLooper()).postDelayed({
            val goTo = Intent(this, MainActivity::class.java)
            startActivity(goTo)
            finish()
        }, 3000)
    }
}