package com.hirin.mytestvidio.ui.base

import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
open class BaseActivity : AppCompatActivity() {
    // <editor-fold defaultstate="collapsed" desc="initialize ui">
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="initialize text">
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="initialize data">
    // </editor-fold>
}