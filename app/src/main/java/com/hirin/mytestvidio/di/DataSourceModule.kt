package com.hirin.mytestvidio.di

import com.hirin.mytestvidio.data.remote.ApiService
import com.hirin.mytestvidio.data.remote.RemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataSourceModule {
    @Singleton
    @Provides
    fun provideRemoteDataSource(
        apiService: ApiService
    ) = RemoteDataSource(apiService)

}