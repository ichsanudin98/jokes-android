package com.hirin.mytestvidio.di

import com.hirin.mytestvidio.data.remote.RemoteDataSource
import com.hirin.mytestvidio.data.repository.DefaultRepository
import com.hirin.mytestvidio.data.repository.SourceRepository
import com.hirin.mytestvidio.utils.AppExecutors
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun provideDefaultRepository(
        remoteDataSource: RemoteDataSource,
        appExecutors: AppExecutors
    ): SourceRepository {
        return DefaultRepository(remoteDataSource, appExecutors)
    }

}